//
//  VA_assignment_1Tests.swift
//  VA-assignment-1Tests
//
//  Created by Frans Glorie on 07/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import XCTest
@testable import VA_assignment_1

class VA_assignment_1Tests: XCTestCase {
    
    // Test variables
    // 2018-02-11
    let testDate = Date(timeIntervalSince1970: 1518350400)
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNormalDate() {
        
        XCTAssert(testDate.toLocalDateString == "11 February 2018")
        
    }
    
}
