//
//  ScheduleViewController.swift
//  VA-assignment-1
//
//  Created by Frans Glorie on 07/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import UIKit

class ScheduleViewController: UIViewController {

    // Outlets
    @IBOutlet weak var labelBeginDate: UILabel!
    @IBOutlet weak var labelEndDate: UILabel!
    @IBOutlet weak var datePickerBeginDate: UIDatePicker!
    
     // Actions
    @IBAction func pbClear(_ sender: UIBarButtonItem) {
        datePickerBeginDate.date = Date()
        updateLabels(using: datePickerBeginDate.date)
    }
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        updateLabels(using: sender.date)
    }
    
    // Functions
    func updateLabels(using beginDate: Date) {
        
        // Update labels
        labelBeginDate.text = beginDate.toLocalDateString
        
        let timeInterval: TimeInterval = 7 * 24 * 3600
        labelEndDate.text = beginDate.addingTimeInterval(timeInterval).toLocalDateString
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Setup controls
        datePickerBeginDate.minimumDate = Date()
        
        // Update labels
        updateLabels(using: datePickerBeginDate.date)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
