//
//  DateExtension.swift
//  VA-assignment-1
//
//  Created by Frans Glorie on 08/02/2018.
//  Copyright © 2018 Frans Glorie. All rights reserved.
//

import Foundation

extension Date {
    
    var toLocalDateString: String {
        
        // Convert date from UTC to local timezone
        let dateFormatter = DateFormatter()
        
        // Inbound date
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        let utcDateString = dateFormatter.string(from: self)

        // Outbound date
        let localDate = dateFormatter.date(from: utcDateString)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = "d MMMM yyyy"
        let strLocalDate = dateFormatter.string(from: localDate!)
        
        return strLocalDate
    }
    
    
}
